<?php

/**
 * Function to return the oai requests
 * 
 */
function _oai_pmh_provider_request(){

  global $base_url;
  global $base_root;

  $compress = TRUE;
  $error = array();
  $xmlOai = array();

  $request = $base_url . '/oai';

  date_default_timezone_set('UTC');
  //$responseDate = date('Y-m-d\Th:i:s\Z');
  $responseDate = gmstrftime('%Y-%m-%dT%H:%M:%SZ',time());

  $args = _oai_pmh_provider_get_args();

  if (isset($args['verb']) && !isset($args['error']) ){
    switch ($args['verb']) {

      case 'GetRecord':
        unset($args['verb']);

        if( !isset($args['identifier']) ){
          $error = array(
            'code' => 'badArgument',
          );
        }elseif( !isset( $args['metadataPrefix'] ) || $args['metadataPrefix']!= 'oai_dc' ){
          $error = array(
            'code' => 'badArgument',
          );
        }else{
          $record = _oai_pmh_provider_GetRecord( $args['identifier'] );
        }

        if( isset( $record['error_code']) ){
          $error['code'] = $record['error_code'];
        }elseif(!isset($error['code'])){
          $xmlOai = array(
            '#theme' => 'oai_pmh_provider_GetRecord',
            '#responseDate' => $responseDate,
            '#request' => $request,
            '#requestType' => NULL,
            '#identifier' => $record['identifier'],
            '#datestamp' => $record['datestamp'],
            '#setSpec' => $record['setSpec'],
            '#metadata' => $record['metadata'],
          );
        }

        break;

      case 'Identify':
        unset($args['verb']);
        // we never use compression in Identify
        $compress = FALSE;

        $xmlOai = _oai_pmh_provider_Identify( $args, $responseDate, $request);

        if( isset($xmlOai['code']) )
          $error = $xmlOai;

        break;

      case 'ListIdentifiers':
        unset($args['verb']);
        /*
        if( !isset($args['from']) ) $args['from'] = NULL;
        if( !isset($args['until']) ) $args['until'] = NULL;
        */
        $xmlOai = _oai_pmh_provider_ListIdentifiers( $args, $responseDate, $request);

        if( isset($xmlOai['code']) )
          $error = $xmlOai; 
        break;

      case 'ListMetadataFormats':
        unset($args['verb']);

        if( isset( $args['identifier']) ){
          if( !_oai_pmh_provider_check_identifier( $args['identifier'] ) ){
            $error['code'] = 'idDoesNotExist';
          }
        }

        if(!$error){
          $xmlOai = array(
            '#theme' => 'oai_pmh_provider_ListMetadataFormats',
            '#responseDate' => $responseDate,
            '#request' => $request,
          );
        }
        
        break;
  
      case 'ListRecords':
        unset($args['verb']);
        $records = _oai_pmh_provider_ListRecords( $args, $responseDate, $request);
     
        if( isset($records['code']) ){
          $error = $records;
        }else{
          $xmlOai = array(
            '#theme' => 'oai_pmh_provider_ListRecord',
            '#responseDate' => $responseDate,
            '#request' => $request,
            '#records' => $records['items'],
            '#resumption_TokenCursor' => $records['#resumption_TokenCursor'],
            '#resumptionToken_completeListSize' => $records['#resumptionToken_completeListSize'],
            '#resumptionToken_value' => $records['#resumptionToken_value'],
          );
        }
        break;

      case 'ListSets':
        unset($args['verb']);
        $xmlOai =_oai_pmh_provider_ListSets($args, $responseDate, $request); 
        break;

      default:
        $error['code'] = 'badVerb';
    } /*switch */
  
  }elseif( isset($args['error']) ){
    $error = $args['error'];
  }else{
    // o argumento verb não foi passado
    $error['code'] = 'badVerb';
  }

  // se der algum erro ...
  if( $error ){
    // we never use compression with errors
    $compress = FALSE; 
    $xmlOai = _oai_pmh_provider_get_error( $request, $responseDate, $error['code'] );
  }

  // ***** display the xml ***** //

  if ($compress && !$error){
    ob_start('ob_gzhandler'); 
  }

  // Set the xml header
  drupal_add_http_header('Content-Type', 'text/xml; charset=utf-8');

  print render($xmlOai);

  if ($compress && !$error){
    ob_end_flush();
  }
  
  drupal_exit();
}

/**
 * OAI-PMH 
 * 
 */
function oai_pmh_provider_request_xml( $content ){

// "<?xml version=\"1.0\" encoding=\"UTF-8\" \n";
  $header = '<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
         http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">

  ' . $content . '
  </OAI-PMH>';
  return $header;
  
}

/**
 * OAI-PMH request header
 * 
 */
function oai_pmh_provider_request_header( $responseDate, $request ){

  $request_header = '<responseDate>' . $responseDate .'</responseDate>';
  $request_header .= '<request verb="Identify">' . $request . '</request>';

  return $request_header;
}

/**
 * Identyfier request
 * 
 * @param array $args
 *  passed in url
 * @return array
 */
function _oai_pmh_provider_Identify( $args, $responseDate, $request ){
  // parse and check args
  if (count($args) > 0){
    foreach ($args as $key=>$val) {
      return array('code' => 'badArgument' );
    }
  }

  $repositoryName = variable_get('oai_pmh_provider_repositoryName',  variable_get('site_name') );
  $baseURL = $request;
  $adminEmail = variable_get('oai_pmh_provider_adminEmail',  variable_get('site_mail') );
  $repositoryIdentifier = variable_get('oai_pmh_provider_repositoryIdentifier');
  $sampleIdentifierNid = oai_pmh_provider_get_sampleIdentifier_nid();
  $sampleIdentifier = 'oai:'.$repositoryIdentifier.':'.$sampleIdentifierNid;

  return array(
    '#theme' => 'oai_pmh_provider_Identify',
    '#responseDate' => $responseDate,
    '#request' => $request,
    '#repositoryName' => $repositoryName,
    '#baseURL' => $baseURL,
    '#adminEmail' => $adminEmail,
    '#earliestDatestamp' => _oai_pmh_provider_get_earliestDatestamp(),
    '#repositoryIdentifier' => $repositoryIdentifier,
    '#sampleIdentifier' => _oai_pmh_provider_xmlstr($sampleIdentifier, 'iso8859-1', FALSE),
  );
}

/**
 * Retorna o request ListSets ou false se der algo errado
 * 
 * @param array $args
 *   Argumentos passados para o servidor oai
 *
 * @return mixed
 */
function _oai_pmh_provider_ListSets( $args, $responseDate, $request ){
  
  $sets = _oai_pmh_provider_get_Sets();
  if( $sets ){
    return  array(
      '#theme' => 'oai_pmh_provider_ListSets',
      '#responseDate' => $responseDate,
      '#request' => $request,
      '#sets' => $sets,
    );
  }

  return FALSE;
}

/**
 * Processa o request ListIdentiers
 * 
 * @return array
 */
function _oai_pmh_provider_GetRecord( $identifier ){

  $identifierPrefix = _oai_pmh_provider_get_identifierPrefix();
  $setVid = oai_pmh_provider_get_set_vid();

  $identifierArray = explode(':',$identifier);

  if( count($identifierArray) == 3 ){
    if(is_numeric( $identifierArray[2] )){
      // check the identifier prefix
      if( $identifierPrefix.$identifierArray[2] != $identifier ){
        return array(
          'error_code' => 'idDoesNotExist',
        );
      }

      // node id
      $nid = $identifierArray[2];

      // load the node
      $node = node_load($nid, NULL, TRUE);

      $oai_content_types = oai_pmh_provider_get_oai_contentTypes();

      // check if node exist
      if( !empty($node) ){

        // if the node content type dont are in selected content types for
        // oai output return a error
        if(!in_array($node->type, $oai_content_types) ){
          return array(
            'error_code' => 'idDoesNotExist',
          );     
        }

        // unique string identifier for oai resource
        $resource['identifier'] = $identifier;

        //print '</pre>';exit;
        
        // created date 
        $resource['datestamp'] = _oai_pmh_provider_format_date($node->created);
        // spec if are avaible
        $resource['setSpec'] = _oai_pmh_provider_get_node_setSpecs($node->nid, array($setVid));
        // Resource title
        $resource['metadata']['dc:title'][] = _oai_pmh_provider_xmlstr($node->title, 'iso8859-1', FALSE);
        // get the elements avaible in the site
        $elements = _oai_pmh_provider_get_elements();
        // fields selected for this content type
        $maped_fields = _oai_pmh_provider_get_map( $node->type );
        // reset the field value
        $field = null;

        foreach( $maped_fields[ $node->type ] as $maped_field ){

          $fieldName = $maped_field['field_machine_name'];

          $element = $elements[ $maped_field['element_id'] ];

          // if adicionado para evitar um erro por mudançar na configuração
          if( $fieldName ){
              
            $fields = field_get_items('node', $node, $fieldName);
            
            $field = NULL;
            $fieldValues = NULL;
            
            if( !empty( $fields ) ){
              // se o campo é uma taxonomia
              if( isset($fields[0]['tid']) ){
                foreach( $fields as $field ){
                  $fieldValues = taxonomy_term_load( $field['tid'] );
                  $resource['metadata'][$element->label][] = $fieldValues->name;
                }
              }else{
              // nao é uma taxonomia
                foreach( $fields as $field ){
                  if( !empty($field['value']) )
                    $resource['metadata'][$element->label][] = _oai_pmh_provider_xmlstr($field['value'], 'iso8859-1', FALSE);
                }             
              }
            }
          }
        }

        return $resource;
      }
    }
  }

  // Erro ao pegar o record ou identifier invalido
  return array(
    'error_code' => 'idDoesNotExist',
  );
}

/**
 * Make the ListRecords resquest
 * 
 * @param array $args
 *  arguments passed in the url
 */
function _oai_pmh_provider_ListRecords($args, $responseDate, $request){

  $records = array();
  $setArray = NULL;
  $setTid = '';
  $tids = array(2);
  $indice = 0;

  if( count($args) < 1 ){
    return array(
      'code' => 'badArgument',
    );
  }

  if( (isset($args['metadataPrefix']) && $args['metadataPrefix'] != 'oai_dc') 
     || (isset($args['resumptionToken']) && isset($args['metadataPrefix']))
     ){
     return array(
      'code' => 'badArgument',
     );
  }

 // parse and check if the from and until are right
  if( isset($args['from']) || isset($args['until'])  ){
    //?verb=ListIdentifiers&metadataPrefix=oai_dc&from=2000-01-01&until=2200-01-01
    $dataCorreta = true;
    if( isset($args['from']) )
      $dataCorreta = _oai_pmh_provider_check_date( $args['from'] );

    if( isset($args['until']) && $dataCorreta )
      $dataCorreta = _oai_pmh_provider_check_date( $args['until'] );

    if( !$dataCorreta ){
      return array(
        'code' => 'badArgument',
      );
    }elseif( isset($args['from']) && isset($args['until']) ){
      if($args['from'] > $args['until']){
        return array(
          'code' => 'noRecordsMatch',
        );
      }
    }
  }

  // If have resumptioToken
  if( isset($args['resumptionToken']) && !isset($args['metadataPrefix']) ){
    $resumption = _oai_pmh_provider_get_values_from_resumptionToken($args['resumptionToken']);

    if( $resumption && $resumption['indice'] > 0 ){

      // parse end check the sets if dont have resumptionToken
      if( isset($resumption['set']) ){
        $setArray = _oai_pmh_provider_parse_get_setArray_from_setSpec( $resumption['set'] );

        if( $setArray ){
          $setTid = $setArray['tid'];
        }
      }

      $between = array( 'from' => '', 'until' => '' );
      $between['from']=( $resumption['from'] )?
        _oai_pmh_provider_convert_datetime_unix( $resumption['from'] ): NULL;
      $between['until']=( $resumption['until'] )?
        _oai_pmh_provider_convert_datetime_unix ( $resumption['until'] ): NULL;

      $records['items'] = _oai_pmh_provider_get_record_list($setTid, $resumption['indice'], $between);

      //print(''); print_r( oai_pmh ); print(''); exit;

      if( $records['items'] ){
      // if have items
        $resumptionTokenArray = _oai_pmh_provider_create_resumptionToken(
          $resumption['indice'],
          $resumption['metadataPrefix'],
          $resumption['from'],
          $resumption['until'],
          $between,
          $setTid
        );

        if( $resumptionTokenArray ){
          $records['#resumption_TokenCursor'] = $resumptionTokenArray['cursor'];
          $records['#resumptionToken_completeListSize'] = $resumptionTokenArray['completeListSize'];
          $records['#resumptionToken_value'] = $resumptionTokenArray['value'];      
        }
        return $records;
      }else{
      // if dont have record items
        return array(
          'code' => 'noRecordsMatch',
        );        
      }
    }else{
      return array(
        'code' => 'badResumptionToken',
      );
    }
    
  // Else dont have resumption token
  }else{

    // if dont exist set null for from and until
    if( !isset($args['from']) ) $args['from'] = NULL;
    if( !isset($args['until']) ) $args['until'] = NULL;

    // parse end check the sets
    if( isset($args['set']) ){
      $setArray = _oai_pmh_provider_parse_get_setArray_from_setSpec( $args['set'] );
      if( !$setArray ){
        return array(
          'code' => 'noRecordsMatch',
        );
      }
      $setTid = $setArray['tid'];
    }

    // From and until args
    $between = array();
    $between['from']=( $args['from'] )?
      _oai_pmh_provider_convert_datetime_unix( $args['from'] ): NULL;
    $between['until']=( $args['until'] )?
      _oai_pmh_provider_convert_datetime_unix ($args['until'] ): NULL;


    $records['items'] = _oai_pmh_provider_get_record_list($setTid, $indice, $between);

    if( $records['items'] ){
      $resumptionTokenArray = _oai_pmh_provider_create_resumptionToken('','oai_dc',$args['from'], $args['until'], $between, $setTid );

      if( $resumptionTokenArray ){
        $records['#resumption_TokenCursor'] = $resumptionTokenArray['cursor'];
        $records['#resumptionToken_completeListSize'] = $resumptionTokenArray['completeListSize'];
        $records['#resumptionToken_value'] = $resumptionTokenArray['value'];      
      }
      return $records;
    }

    return array(
      'code' => 'noRecordsMatch',
    );
  }
}

/**
 * Processa o request ListIdentiers
 * 
 * 
 * 
 * @return mixed
 */
function _oai_pmh_provider_ListIdentifiers($args, $responseDate, $request){
  $setArray = NULL;
  $tids = array( oai_pmh_provider_get_set_vid() );
  $indice = 0;
  $setArray = array();
  $setTid = '';
  
  if( count($args) < 1 ){
    return array(
      'code' => 'badArgument',
    );
  }

  if( (isset($args['metadataPrefix']) && $args['metadataPrefix'] != 'oai_dc')
     || (isset($args['resumptionToken']) && isset($args['metadataPrefix']))
     ){
     return array( 
      'code' => 'badArgument',
     );
  }

  // parse and check if the from and until are right
  if( isset($args['from']) || isset($args['until'])  ){

    $dataCorreta = true;
    if( isset($args['from']) )
      $dataCorreta = _oai_pmh_provider_check_date( $args['from'] );

    if( isset($args['until']) && $dataCorreta )
      $dataCorreta = _oai_pmh_provider_check_date( $args['until'] );

    if( !$dataCorreta ){
      return array(
        'code' => 'badArgument',
      );
      
    }elseif( isset($args['from']) && isset($args['until']) ){
      if($args['from'] > $args['until']){
        return array(
          'code' => 'noRecordsMatch',
        );
      }
    }
    
  }
  // if have resumptionToken
  if( isset($args['resumptionToken']) && !isset($args['metadataPrefix']) ){
    $resumption = _oai_pmh_provider_get_values_from_resumptionToken($args['resumptionToken']);
    //print_r($resumption);exit;
    if( $resumption && $resumption['indice'] > 0 ){

      // parse end check the sets if dont have resumptionToken
      if( $resumption['set'] ){
        $setArray = _oai_pmh_provider_parse_get_setArray_from_setSpec( $resumption['set'] );

        if( $setArray ){
          $setTid = $setArray['tid'];
        }
      }

      $between = array( 'from' => '', 'until' => '' );
      $between['from']=( $resumption['from'] )?
        _oai_pmh_provider_convert_datetime_unix( $resumption['from'] ): NULL;
      $between['until']=( $resumption['until'] )?
        _oai_pmh_provider_convert_datetime_unix ( $resumption['until'] ): NULL;


      $ListIdentifiers = _oai_pmh_provider_get_identifiersList(
        $setArray,
        $resumption['indice'],
        $between
      );
      
      if( count($ListIdentifiers) ){

        $resumptionTokenArray = _oai_pmh_provider_create_resumptionToken(
          $resumption['indice'],
          $resumption['metadataPrefix'],
          $resumption['from'],
          $resumption['until'],
          $between,
          $setTid
        );

        return array(
          '#theme' => 'oai_pmh_provider_ListIdentifiers',
          '#responseDate' => $responseDate,
          '#request' => $request,
          '#requestType' => 'resumptionToken="'.$args['resumptionToken'].'"',
          '#ListIdentifiers' => $ListIdentifiers,
          '#resumption_TokenCursor' => $resumptionTokenArray['cursor'],
          '#resumptionToken_completeListSize' => $resumptionTokenArray['completeListSize'],
          '#resumptionToken_value' => $resumptionTokenArray['value'],
        );
      }else{
        return array(
          'code' => 'noRecordsMatch',
        );        
      }
    }else{
      return array(
        'code' => 'badResumptionToken',
      );
    }
  // dont have resumption token
  }else{
    
    // seta the args['from'] and args['until'] to NULL to avoid errors
    if( !isset($args['from']) ) $args['from'] = NULL;
    if( !isset($args['until']) ) $args['until'] = NULL;

    // set the between from id have args['from']
    $between['from']=( $args['from'] )?
      _oai_pmh_provider_convert_datetime_unix( $args['from'] ): NULL;
    $between['until']=( $args['until'] )?
      _oai_pmh_provider_convert_datetime_unix ($args['until'] ): NULL;
    
    // parse end check the sets if dont have resumptionToken
    if( isset($args['set']) ){
      $setArray = _oai_pmh_provider_parse_get_setArray_from_setSpec( $args['set'] );
      if( !$setArray ){
        return array(
          'code' => 'noRecordsMatch',
        );
      }

      $setTid = $setArray['tid'];
    }

    // get the identifiers list
    $ListIdentifiers = _oai_pmh_provider_get_identifiersList( $setArray, $indice, $between );
    if( $ListIdentifiers ){
      $resumptionTokenArray = _oai_pmh_provider_create_resumptionToken(
        '',
        'oai_dc',
        $args['from'],
        $args['until'],
        $between,
        $setTid
      );
      
      $result = array(
        '#theme' => 'oai_pmh_provider_ListIdentifiers',
        '#responseDate' => $responseDate,
        '#request' => $request,
        '#requestType' => 'metadataPrefix="'.$args['metadataPrefix'].'"',
        '#ListIdentifiers' => $ListIdentifiers,
      ); 
      //print_r( $resumptionTokenArray ); exit;
      if( $resumptionTokenArray ){
        $result['#resumption_TokenCursor'] = $resumptionTokenArray['cursor'];
        $result['#resumptionToken_completeListSize'] = $resumptionTokenArray['completeListSize'];
        $result['#resumptionToken_value'] = $resumptionTokenArray['value'];      
      }
      return $result;
    }
    return array(
      'code' => 'noRecordsMatch',
    );
  }
  
  // id have the $error variable
  // @TODO need remove this if
  if($error)
    return array(
      'code' => $error['code'],
    );
}

/**
 * Pega os valores do resumpitionToken
 * 
 * @param string $resumptionToken
 *  string with resunptionToken passed in url 
 * @return mixed
 */
function _oai_pmh_provider_get_values_from_resumptionToken( $resumptionToken ){
  $items = explode(':', $resumptionToken);

  $args_in_resumptionToken = array();
  // resumption toke are 5 params 
  if( count($items) == 5 ){

    // change the setTid for oai setSpec
    $setTid = oai_pmh_provider_get_cvspSet_name_from_tid( $items[1] );
    // set vid
    $vid = oai_pmh_provider_get_set_vid();
    // load the vocabulary
    $vocabulary = taxonomy_vocabulary_load($vid);
    // make the set
    $set = $vocabulary->machine_name . ':' . $setTid;

    // Return the args in resumptionToken
    $args_in_resumptionToken = array(
      'metadataPrefix' => $items[0],
      'set' => $set,
      'from' => $items[2],
      'until' => $items[3],
      'indice' => $items[4],
    );
  }
  return $args_in_resumptionToken;
}

/**
 * Make a resumption token
 * 
 * @param int $indiceAtual
 * @param string $metadataPrefix
 * @param string $from
 * @param string $until
 * @param array $between
 * @param int $setTid
 * @param int $numeroDeItems
 * 
 * @return string
 *  Resumption token
 */
function _oai_pmh_provider_create_resumptionToken( $indiceAtual = 0, $metadataPrefix = 'oai_dc',
  $from = '', $until = '', $between = array(), $setTid = '' ,$numeroDeItems = 100){

  // next page
  $indiceProximaLista = $numeroDeItems + $indiceAtual;
        
  // get the nodes list size from database
  $query = oai_phm_provider_get_completeList_size($setTid, NULL, $between);

  $completeListSize = $query->fetchAll();
  $completeListSize = $completeListSize[0]->expression;

  $cursor = $indiceAtual/$numeroDeItems;

  // print '<pre>'; print_r( $completeListSize . ' - ' . $numeroDeItems . '-' . $indiceAtual ); print '</pre>'; exit;
  
  if( $completeListSize > $indiceProximaLista &&
    $completeListSize >= $indiceAtual ){
    return array(
      'cursor' => $cursor,
      'completeListSize' => $completeListSize,
      'value' =>  $metadataPrefix.':'.$setTid.':'. $from .':' . $until . ':' . $indiceProximaLista,
    );
  }
  
  return false;
}

/**
 * Pega a lista de identificadores ?verb=ListIdentifiers
 *
 * @param array $tids
 *  Array com os tids das taxonomias selecionadas para o list sets
 * @param int $indice
 *  Número para iniciar o range de nodes a retornar
 * @return array
 */
function _oai_pmh_provider_get_identifiersList($setArray, $indice=0, $between=array()){
  $identifiers = array();
  $setTid = NULL;
  $setVid = oai_pmh_provider_get_set_vid();
  
  if( isset($setArray['tid']))
    $setTid = $setArray['tid'];

  $identifierPrefix = _oai_pmh_provider_get_identifierPrefix();
  $nodes = _oai_pmh_provider_get_nodes( $setTid, $indice, $between ); 

  foreach( $nodes as $key => $node){
    $identifiers[$node->nid]['identifier'] =  $identifierPrefix . $node->nid;
    $identifiers[$node->nid]['datestamp'] = _oai_pmh_provider_format_date($node->changed);
    $identifiers[$node->nid]['setSpecs'] = _oai_pmh_provider_get_node_setSpecs($node->nid, array($setVid));
  }

  return $identifiers;
}

/**
 * Pega a lista de identificadores ?verb=ListIdentifiers
 *
 * @param array $tids
 *  Array com os tids das taxonomias selecionadas para o list sets
 * @param int $indice
 *  Número para iniciar o range de nodes a retornar
 * @return array
 */
function _oai_pmh_provider_get_record_list($setArray=NULL, $indice=0, $between=array()){

  $identifiers = array();
  $setTid = NULL;

  if( isset($setArray['tid']))
    $setTid = $setArray['tid'];

  // get the identifier prefix
  $identifierPrefix = _oai_pmh_provider_get_identifierPrefix();
  // get the nodes
  $nodes = _oai_pmh_provider_get_nodes( $setTid, $indice, $between ); 

  foreach($nodes as $key => $node){
    $records[$node->nid] =  _oai_pmh_provider_GetRecord( $identifierPrefix.$node->nid );
  }

  return $records;
}

/**
 * The function missing from D7
 * Taxonomy node get terms
 */
function _oai_pmh_provider_get_node_setSpecs($nid, $vid=array() , $key='tid') {
  $terms = array();
  $query = db_select('taxonomy_index', 'r');

  $cvspSets = oai_pmh_provider_get_cvspSets();

  $query->join('taxonomy_term_data', 't', 'r.tid = t.tid');
  $query->join('taxonomy_vocabulary', 'v', 't.vid = v.vid');
  $query->fields( 't', array('tid') );
  $query->fields( 'v', array('machine_name') );

  if( $vid )
    $query->condition("v.vid", $vid , "in");

  $query->condition("r.nid", $nid);

  $result = $query->execute();

  foreach ($result as $term) {
    $terms[$term->$key] = $term->machine_name . ':' . $cvspSets[$term->tid] ;
  }

  return $terms;
}

/**
 * Get the sets for oai ListSets
 *
 * @return array
 * @TODO preciso adcionar a capacidade de retornar e criar as sub taxonomias
 */
function _oai_pmh_provider_get_Sets(){
  
  $sets = array();
  $vid = oai_pmh_provider_get_set_vid();
  $vocabulary = taxonomy_vocabulary_load($vid);
  $tems = taxonomy_get_tree($vid);

  $cvspSets = oai_pmh_provider_get_cvspSets();


  foreach( $tems as $term){
    $setKey = $vocabulary->machine_name .':'. $term->tid;
    $sets[ $setKey ]['setSpec'] = $vocabulary->machine_name .':'. $cvspSets[$term->tid];
    $sets[ $setKey ]['setName'] = $term->name;
  }

  return $sets;
}

/**
 * Parse and if the set are correct return the tid and vocabulary_machine_name
 * @param string $set
 *  Set no formato oai_type:13 (vocabulary_machine_name:tid)
 * @return mixed
 */
function _oai_pmh_provider_parse_get_setArray_from_setSpec( $set ){

  $term = NULL;
  $setArray = explode(':',$set);
  //$setTid = oai_pmh_provider_get_set_vid();

  if( count($setArray) == 2 ){
    
    $setTid = oai_pmh_provider_get_cvspSet_id( $setArray[1] );

    if( $term = taxonomy_term_load( $setTid ) ){
      if( $term->vocabulary_machine_name == $setArray[0] ){
        return array(
          'vocabulary_machine_name' => $setArray[0],
          'tid' => $setTid
        );              
      }
    }
  }
  // return false id the set dont validate
  return false;
}

/**
 * verifica se um identifier esta correto
 * @param string $identifier
 *  identifier a ser verificado
 * @return bool
 */
function _oai_pmh_provider_check_identifier( $identifier ){

  $identifierPrefix = _oai_pmh_provider_get_identifierPrefix();

  $identifierArray = explode(':',$identifier);
  if( count($identifierArray) == 3 ){
    if(is_numeric( $identifierArray[2] )){
      if( $identifierPrefix.$identifierArray[2] == $identifier ){
        return true;
      }
    }
  }

  return false;
}

/**
 * Retorna o identifierPrefix do repositorio
 * 
 * @return string
 */
function _oai_pmh_provider_get_identifierPrefix(){
  return 'oai:'.variable_get('oai_pmh_provider_repositoryIdentifier'). ':';
}

/**
 * Get the last changed node
 *
 * @return string;
 */
function _oai_pmh_provider_get_earliestDatestamp(){
  $datestamp = db_query("SELECT min(created) FROM {node} ")->fetchField();
  // return gmstrftime('%Y-%m-%dT%H:%M:%SZ', $datestamp);
  return gmstrftime('%Y-%m-%d', $datestamp);
}

/**
 * Retorna o array de com o tema e variaveis setadas para enviar para o navegador
 *
 * @param string $request
 *  Url do request
 * @param string $responseDate
 *  Data em que o request foi criado
 * @param string $code
 *  Codigo de erro
 * @param string $argument
 *  Argumento usado pelo usuário
 * @param string @value
 *  Valor do argumento usado pele usuário
 * @return array;
 */
function _oai_pmh_provider_get_error( $request, $responseDate, $code, $argument='', $value=''){

  switch ($code) {
    case 'badArgument' :
      $error = t("The request includes illegal arguments, is missing required arguments, repeats arguments or the value for an argument has an illegal syntax.");
      break;
    case 'badGranularity' :
      $error = t("The value @value of the argument '@argument' is not valid.",array('@value' => $value, '@argument' =>$argument));
      $code = 'badArgument';
      break;
    case 'badResumptionToken' :
      $error = t("The resumptionToken @value does not exist or has already expired.",array('@value' => $value));
      break;
    case 'badRequestMethod' :
      $error = t("The request method @argument is unknown.",array('@argument' =>$argument));
      $code = 'badVerb';
      break;
    case 'badVerb' :
      $error = t("The verb @argument provided in the request is illegal.",array('@argument' =>$argument));
      $code = 'badVerb';
      break;
    case 'cannotDisseminateFormat' :
      $error = t("The metadata format @value given by @argument is not supported by this repository.",array('@value' => $value, '@argument' =>$argument));
      $code = 'cannotDisseminateFormat';
      break;
    case 'exclusiveArgument' :
      $error = t('The usage of resumptionToken as an argument allows no other arguments.');
      $tipo = 'badArgument';
      break;
    case 'idDoesNotExist' :
      $error = t("The value @value of the identifier is illegal for this repository.",array('@value' => $value));
      break;
    case 'missingArgument' :
      $error = t("The required argument @argument is missing in the request.",array('@argument' =>$argument));
      $code = 'badArgument';
      break;
    case 'noRecordsMatch' :
      $error = t('The combination of the given values results in an empty list.');
      break;
    case 'noMetadataFormats' :
      $error = t('There are no metadata formats available for the specified item.');
      break;
    case 'noVerb' :
      $error = t('The request does not provide any verb.');
      $code = 'badVerb';
      break;
    case 'noSetHierarchy' :
      $error = t('This repository does not support sets.');
      break;
    case 'sameArgument' :
      $error = t('Do not use them same argument more than once.');
      $code = 'badArgument';
      break;
    case 'sameVerb' :
      $error = t('Do not use verb more than once.');
      $code = 'badVerb';
      break;
    default:
      $error = t("Unknown error: code: @code, argument: @argument, value: @value",array('@value' => $value, '@argument' =>$argument,'@code' => $code));
      $code = 'badArgument';
  }

  return array(
    '#theme' => 'oai_pmh_provider_error',
    '#responseDate' => $responseDate,
    '#request' => $request,
    '#error' => _oai_pmh_provider_xmlstr($error, 'iso8859-1', FALSE),
    '#code' => $code,
  );
}

////// XML \\\\\\
/**
 * Trata e retorna uma string para enciar no OAI.
 *
 * @param string $xmlescaped
 * @param string $charset
 * @param string $string
 * @return string
 *
 */
function _oai_pmh_provider_xmlstr($string, $charset = 'iso8859-1', $xmlescaped = 'false'){
  $xmlstr = stripslashes(trim($string));
  // just remove invalid characters
  $pattern ="/[\x-\x8\xb-\xc\xe-\x1f]/";
  $xmlstr = preg_replace($pattern, '', $xmlstr);

  // escape only if string is not escaped
  if (!$xmlescaped) {
    $xmlstr = htmlspecialchars($xmlstr, ENT_QUOTES);
  }

  return $xmlstr;
}
/**
 * 
 * takes either an array or a string and outputs them as XML entities
 * 
 */
function _oai_pmh_provider_xmlformat($record, $element, $attr = '', $indent = 0){
  $charset = 'iso8859-1';
  $xmlescaped = 'false';
    
  if ($attr != '') {
    $attr = ' '.$attr;
  }

  $str = '';
  if (is_array($record)){
    foreach  ($record as $val) {
      $str .= str_pad('', $indent).'<'.$element.$attr.'>'._oai_pmh_provider_xmlstr($val, $charset, $xmlescaped).'</'.$element.">\n";
    }
    return $str;
  } elseif ($record != '') {
    return str_pad('', $indent).'<'.$element.$attr.'>'._oai_pmh_provider_xmlstr($record, $charset, $xmlescaped).'</'.$element.">\n";
  } else {
    return '';
  }
}
////// FIM XML \\\\\\\

/**
 * Verifica se uma data é válida
 *
 * @param string $date
 *  data que vai ser verificada no formato YYYY-MM-DD
 * @return mixed
 */
function _oai_pmh_provider_check_date( $date ){
    
  if( $dateArray = explode('-', $date ) ){
  /*  if( count( $dateArray ) == 3 &&    
      count( (string)$dateArray[2] ) == 2 &&
      count( (string)$dateArray[1] ) == 2 &&
      count( (string)$dateArray[0] ) == 4 ){
*/
    if( count( $dateArray ) == 3 &&
      is_numeric($dateArray[2]) &&
      is_numeric($dateArray[1]) &&
      is_numeric($dateArray[0]) ){
        
      if( checkdate($dateArray['1'], (string)$dateArray['2'], $dateArray['0']) ){
        return true;
      }
    }
  }
  return false;
}

function _oai_pmh_provider_convert_datetime_unix($date){
  list($year, $month, $day) = explode('-', $date);

  //list($hour, $minute, $second) = explode(':', $time);
  $hour= '0';
  $minute = '0';
  $second = '0';

  return mktime($hour, $minute, $second, $month, $day, $year);

}

/**
 * 
 * @param int $unixDate
 *  date in unix time
 * @return strig
 */
function _oai_pmh_provider_format_date($unixDate){
  // return gmstrftime('%Y-%m-%dT%H:%M:%SZ', $datestamp);
  return gmstrftime('%Y-%m-%d', $unixDate);
}

/**
 * Verifica se os args apssados são validos e retorna os args se tudo certo
 * 
 * 
 */
function _oai_pmh_provider_get_args( ){

  if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $args = $_GET;
    $getarr = explode('&', $_SERVER['QUERY_STRING']);
  } elseif ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $args = $_POST;
  } else {
    $args['error'] = array(
      'code' => 'badRequestMethod'
    );
  }
  // remove o arg q do drupal
  unset($args['q']);
  
  // and now we make the OAI Repository Explorer really happy
  // I have not found any way to check this for POST requests.
  if (isset($getarr)) {
    if (count($getarr) != count($args)) {
      $args['error'] = array(
        'code' => 'sameArgument'
      );      
    }
  }

  //Se der erro retorna os args com o indice ['error']
  //$param = $_GET;
  return $args;
}

/**
 * Get the set vid from drupal settings
 * @return array
 */
function oai_pmh_provider_get_set_vid(){

  return variable_get('oai_pmh_provider_set',array());
}

/***** CVSP unique funcions to use the default cvsp terms
******

/**
 * Os sets do CVSP
 * 
 * @return array
 */
function oai_pmh_provider_get_cvspSets( ){
  // sets enviados pelo cvsp
  $setsCVSP = array(
    11225 => 'Course' ,
    11222 => 'LearningObject',
    11223 => 'LearningTool',
    11224 => 'Research',
  );
  
  return $setsCVSP;
}

/**
 * Get the term id fron cvsp set
 * 
 * @param string $setName
 * @return int
 *  Return  the cvsp setIp
 */
function oai_pmh_provider_get_cvspSet_id( $setName ){
    
  $cvsp_setId = NULL;
  $sets = oai_pmh_provider_get_cvspSets();
  
  foreach( $sets as $setId => $set ){
    if( $set == $setName ){
      $cvsp_setId = $setId;
      break;  
    }
  }
  return $cvsp_setId;
}

/**
 * Return a cvspSet name
 * 
 * @param int $setTid
 *  set tid 
 * @return string
 */
function oai_pmh_provider_get_cvspSet_name_from_tid( $setTid ){
  if( $setTid ){
    $sets = oai_pmh_provider_get_cvspSets();
    return $sets[$setTid];
  }
  return ''; 
}

/////******* END of CVSP unique finctio 

/**
 * Get one nid for sample identify
 * 
 * @return string
 */
function oai_pmh_provider_get_sampleIdentifier_nid(){

  $result = _oai_pmh_provider_get_nodes(array(), 0, array());
  foreach( $result as $node ){
    $nid = $node->nid;
  }

  return $nid;
}


/**
 * 
 */
function oai_phm_provider_get_completeList_size($setTid, $indice=0, $between=array()){

  $oai_content_types = oai_pmh_provider_get_oai_contentTypes();

  try{
    $query = db_select('node', 'n')
      ->condition('n.status', 0,'<>');
    $query->addExpression('COUNT(n.nid)');
    // if have a set 
    if( $setTid ){
     $query->join('taxonomy_index', 'ti', 'n.nid = ti.nid ');
     $query->condition('ti.tid', $setTid ,'=');
    }

    // content type selected for oai output
    $query->condition('n.type', $oai_content_types ,'in');

    // if have a arg from or(and) arg until
    if( $between ){
      if( $between['from'] && $between['until'] )
        $query->condition('n.changed', array( $between['from'], $between['until'] ), 'BETWEEN');
      elseif($between['from'])
        $query->condition('n.changed',$between['from'] ,'>=');
      elseif($between['until'])
        $query->condition('n.changed',$between['until'],'<=');
    }

    return $query->execute(); 

  }catch (PDOException $ex){
    watchdog( "oai_pmh_provider", "SQL Error ". $ex->getMessage().", Stmt: " . $query->__toString(), NULL, WATCHDOG_ERROR );
  }
}

// exemplo de como modificar a saida do xml em outro modulo
/*
function oai_pmh_provider_preprocess(&$variables, $hook){
  if($hook == 'oai_pmh_provider_Identify'){
    print '<pre>';print_r($hook);print '</pre>';
    print '<pre>';print_r($variables);print '</pre>';
    exit;
  }
}
*/
