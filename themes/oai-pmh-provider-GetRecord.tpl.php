<?php
 /**
  *
  * <?php print $responseDate; ?>
  * <?php print $request; ?>
  * <?php print $requestType; ?>
  * <?php print $identifier; ?>
  * 
  * <?php print $datestamp; ?>
  * 
  * $setSpec = array();
  * 
  * Implementar
  * <?php print $identifier; ?>
  */
?>
<?php print "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"; ?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
         http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
  <responseDate><?php print $responseDate; ?></responseDate>
  <request identifier="<?php print $identifier; ?>" <?php print $requestType; ?> metadataPrefix="oai_dc" verb="GetRecord"><?php print $request; ?></request>
  <GetRecord>
  <record>
    <header>
      <identifier><?php print $identifier; ?></identifier>
      <datestamp><?php print $datestamp; ?></datestamp>
      <?php foreach ( $setSpec as $set): ?>
      <setSpec><?php print $set; ?></setSpec>
      <?php endforeach; ?>
    </header>
    <metadata>
      <oai_dc:dc
          xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
          xmlns:dc="http://purl.org/dc/elements/1.1/"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/
                              http://www.openarchives.org/OAI/2.0/oai_dc.xsd">                             
        <?php foreach ( $metadata as $key=>$datas): ?>
        <?php foreach ( $datas as $data): ?>
          <<?php print $key; ?>><?php print $data; ?></<?php print $key; ?>>
        <?php endforeach; ?>
        <?php endforeach; ?>
      </oai_dc:dc>
    </metadata>
  </record>
 </GetRecord>
</OAI-PMH>
