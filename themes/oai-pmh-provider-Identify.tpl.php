<?php
 /**
  *
  * <?php print $responseDate; ?>
  * <?php print $request; ?>
  * <?php print $repositoryName; ?>
  * <?php print $baseURL; ?>
  * <?php print $adminEmail; ?>
  * <?php print $earliestDatestamp; ?>
  * <?php print $repositoryIdentifier; ?>
  *
  * implementar no futuro
  *

  * <?php print $delimiter; ?>
  * <?php print $sampleIdentifier; ?>
  * <?php print $title; ?>
  *
  */
?>
<?php print "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"; ?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
         http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
 <responseDate><?php print $responseDate; ?></responseDate>
 <request verb="Identify"><?php print $request; ?></request>
 <Identify>
    <repositoryName><?php print $repositoryName; ?></repositoryName>
    <baseURL><?php print $baseURL; ?></baseURL>
    <protocolVersion>2.0</protocolVersion>
    <adminEmail><?php print $adminEmail; ?></adminEmail>
    <earliestDatestamp><?php print $earliestDatestamp; ?></earliestDatestamp>
    <deletedRecord>no</deletedRecord>
    <granularity>YYYY-MM-DD</granularity>
    <compression>gzip</compression>
		<description>
			<oai-identifier xmlns="http://www.openarchives.org/OAI/2.0/oai-identifier"
                      xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai-identifier
                      http://www.openarchives.org/OAI/2.0/oai-identifier.xsd">
				<scheme>oai</scheme>
				<repositoryIdentifier><?php print $repositoryIdentifier; ?></repositoryIdentifier>
				<delimiter>:</delimiter>
				<sampleIdentifier><?php print $sampleIdentifier; ?></sampleIdentifier>
			</oai-identifier>
		</description>
    <description>
      <toolkit xsi:schemaLocation="http://oai.dlib.vt.edu/OAI/metadata/toolkit http://oai.dlib.vt.edu/OAI/metadata/toolkit.xsd" xmlns="http://oai.dlib.vt.edu/OAI/metadata/toolkit">
        <title>Modulo OAI PMH Provider para Drupal</title>
        <author>
          <name>Alberto Souza</name>
          <email>asjunior@ensp.fiocruz.br</email>
          <institution>ENSP</institution>
        </author>
        <version>1</version>
        <toolkitIcon>http://drupal.org/files/druplicon.small_.png</toolkitIcon>
        <URL><?php print $baseURL; ?></URL>
      </toolkit>
    </description>
  </Identify>
</OAI-PMH>