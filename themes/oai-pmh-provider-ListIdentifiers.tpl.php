<?php
 /**
  *
  * <?php print $responseDate; ?>
  * <?php print $request; ?>
  * <?php print $requestType; ?> 
  * <?php print $resumptionToken; ?>
  *	$ListIdentifiers = array();
  *
  * <?php print $resumption_TokenCursor; ?>
  * <?php print $resumptionToken_completeListSize; ?> 
  * <?php print $resumptionToken_value; ?>
  */
?>
<?php print "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"; ?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
         http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
  <responseDate><?php print $responseDate; ?></responseDate>
  <request <?php print $requestType; ?> verb="ListIdentifiers"><?php print $request; ?></request>
  <ListIdentifiers>
  <?php foreach( $ListIdentifiers as $ListItem ): ?>
    <header>
      <identifier><?php print $ListItem['identifier']; ?></identifier>
      <datestamp><?php print $ListItem['datestamp']; ?></datestamp>
      <?php foreach( $ListItem['setSpecs'] as $setSpec ): ?>
      <setSpec><?php print $setSpec; ?></setSpec>
      <?php endforeach; ?>
		</header>
  <?php endforeach;?>
<?php if( $resumptionToken_value ): ?>
  <resumptionToken cursor="<?php print $resumption_TokenCursor; ?>" completeListSize="<?php print $resumptionToken_completeListSize; ?>"><?php print $resumptionToken_value; ?></resumptionToken>
<?php endif; ?>
  </ListIdentifiers>
</OAI-PMH>

