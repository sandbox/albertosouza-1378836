<?php
 /**
  *
  * <?php print $responseDate; ?>
  * <?php print $request; ?>
  * <?php print $requestType; ?>
  * <?php print $identifier; ?>
  * 
  * <?php print $datestamp; ?>
  
  * $records = array();
  * $setSpec = array();
  * 
  * Implementar
  * <?php print $identifier; ?>
  */
?>
<?php print "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"; ?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
         http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
  <responseDate><?php print $responseDate; ?></responseDate>
  <request <?php print $requestType; ?> metadataPrefix="oai_dc" verb="ListRecords"><?php print $request; ?></request>
 <ListRecords>
 <?php foreach ( $records as $record): ?> 
   <record>
    <header>
      <identifier><?php print $record['identifier']; ?></identifier>
      <datestamp><?php print $record['datestamp']; ?></datestamp>
      <?php foreach ( $record['setSpec'] as $set): ?>
      <setSpec><?php print $set; ?></setSpec>
      <?php endforeach; ?>
    </header>
    <metadata>
      <oai_dc:dc
          xmlns:oai_dc="http://www.openarchives.org/OAI/2.0/oai_dc/"
          xmlns:dc="http://purl.org/dc/elements/1.1/"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/oai_dc/
                              http://www.openarchives.org/OAI/2.0/oai_dc.xsd">                             
        <?php foreach ( $record['metadata'] as $key=>$datas): ?>
          <?php foreach ( $datas as $data): ?>
            <<?php print $key; ?>><?php print $data; ?></<?php print $key; ?>>
          <?php endforeach; ?>
        <?php endforeach; ?>
      </oai_dc:dc>
    </metadata>
  </record>
  <?php endforeach; ?>
<?php if( $resumptionToken_value ): ?>
  <resumptionToken cursor="<?php print $resumption_TokenCursor; ?>" completeListSize="<?php print $resumptionToken_completeListSize; ?>"><?php print $resumptionToken_value; ?></resumptionToken>
<?php endif; ?>
 </ListRecords>
</OAI-PMH>
