<?php
 /**
  *
  * <?php print $responseDate; ?>
  * <?php print $request; ?>
  * $sets array com os set contendo setSpec e setName, para retirar os valores é nescessario usar o foreach Ex:
  *   <?php foreach ($sets as $set): ?>
  *  	<set>
  *  	<setSpec><?php print $set['setSpec']; ?></setSpec>
  *  	<setName><?php print $set['setName']; ?></setName>
  *		</set>
  *	  <?php endforeach; ?>.
  */
?>
<?php print "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"; ?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://www.openarchives.org/OAI/2.0/
         http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd">
 <responseDate><?php print $responseDate; ?></responseDate>
 <request verb="ListSets"><?php print $request; ?></request>
  <ListSets>
  <?php foreach ($sets as $set): ?>
    <set>
    <setSpec><?php print $set['setSpec']; ?></setSpec>
    <setName><?php print $set['setName']; ?></setName>
   </set>
  <?php endforeach; ?>
  </ListSets>
</OAI-PMH>
